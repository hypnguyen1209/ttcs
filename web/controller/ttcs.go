package controller

import (
	"github.com/gin-gonic/gin"
)

type TTCSController struct {
	BaseController

	inboundController *InboundController
	settingController *SettingController
}

func NewTTCSController(g *gin.RouterGroup) *TTCSController {
	a := &TTCSController{}
	a.initRouter(g)
	return a
}

func (a *TTCSController) initRouter(g *gin.RouterGroup) {
	g = g.Group("/ttcs")
	g.Use(a.checkLogin)

	g.GET("/", a.index)
	g.GET("/inbounds", a.inbounds)
	g.GET("/setting", a.setting)

	a.inboundController = NewInboundController(g)
	a.settingController = NewSettingController(g)
}

func (a *TTCSController) index(c *gin.Context) {
	html(c, "index.html", "Dashboard", nil)
}

func (a *TTCSController) inbounds(c *gin.Context) {
	html(c, "inbounds.html", "Inbound", nil)
}

func (a *TTCSController) setting(c *gin.Context) {
	html(c, "setting.html", "Setting", nil)
}
